db.fruits.aggregate([

	{$match: {supplier: "Red Farms Inc."}},
	{$count: "ItemsSupplied"}

	])

db.fruits.aggregate([
	
	{$match: {price: {$gt:50}}},
	{$count: "ItemsGreaterThan50"}

	])

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id:"$supplier", avgPrice: {$avg: "$price"}}}

	])

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id:"$supplier", HighestPrice: {$max: "$price"}}}

	])

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id:"$supplier", lowestPrice: {$min: "$price"}}}

	])